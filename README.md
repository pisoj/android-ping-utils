Android Ping Utils
==================

*Utilities to test network connections*

Set of network utilities to test network connection, helpfull for vpn route testing via traceroute, dns, ip info, dig, ping system utilities...

System information
==================

Changes in Android 10

On devices running Android 9 and lower, the DNS resolver code is spread across Bionic and netd. DNS lookups are centralized in the netd daemon to allow for system-wide caching, while apps call functions (such as getaddrinfo) in Bionic. The query is sent over a UNIX socket to /dev/socket/dnsproxyd to the netd daemon, which parses the request and calls getaddrinfo again to issue DNS lookups, then caches the results so that other apps can use them. The DNS resolver implementation was mostly contained in bionic/libc/dns/ and partly in system/netd/server/dns.

Android 10 moves the DNS resolver code to system/netd/resolv, converts it to C++, then modernizes and refactors the code. The code in Bionic continues to exist for app compatibility reasons, but is no longer called by the system. These source paths are affected by the refactoring:

    bionic/libc/dns
    system/netd/client
    system/netd/server/dns
    system/netd/server/DnsProxyListener.*
    system/netd/resolv

* https://source.android.com/devices/architecture/modular-system/dns-resolver

Screenshots
===========

![shot](/metadata/screenshots/shot1.png)
