package com.github.axet.pingutils.activities;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import com.github.axet.androidlibrary.preferences.AboutPreferenceCompat;
import com.github.axet.androidlibrary.widgets.DotsTabView;
import com.github.axet.pingutils.R;
import com.github.axet.pingutils.fragments.AboutFragment;
import com.github.axet.pingutils.fragments.DigFragment;
import com.github.axet.pingutils.fragments.InfoFragment;
import com.github.axet.pingutils.fragments.MonitorFragment;
import com.github.axet.pingutils.fragments.PingFragment;
import com.github.axet.pingutils.fragments.RouteFragment;
import com.github.axet.pingutils.fragments.SettingsFragment;
import com.github.axet.pingutils.widgets.StatusTabView;

import java.util.Set;

public class MainActivity extends AppCompatActivity {

    public TabLayout tabs;

    public static int STATUS_TAB_INDEX = 0;

    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        private final Context mContext;

        public SectionsPagerAdapter(Context context, FragmentManager fm) {
            super(fm);
            mContext = context;
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return MonitorFragment.newInstance();
                case 1:
                    return InfoFragment.newInstance();
                case 2:
                    return DigFragment.newInstance();
                case 3:
                    return RouteFragment.newInstance();
                case 4:
                    return PingFragment.newInstance();
                case 5:
                    return AboutFragment.newInstance();
            }
            return null;
        }

        @Nullable
        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return StatusTabView.DOTS;
                case 1:
                    return "INFO";
                case 2:
                    return "DIG";
                case 3:
                    return "ROUTE";
                case 4:
                    return "PING";
                case 5:
                    return DotsTabView.DOTS;
            }
            return "";
        }

        @Override
        public int getCount() {
            return 6;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        SectionsPagerAdapter sectionsPagerAdapter = new SectionsPagerAdapter(this, getSupportFragmentManager());
        ViewPager viewPager = (ViewPager) findViewById(R.id.view_pager);
        viewPager.setAdapter(sectionsPagerAdapter);
        tabs = (TabLayout) findViewById(R.id.tabs);
        tabs.setupWithViewPager(viewPager);
        viewPager.setCurrentItem(1);
        StatusTabView.update(tabs, 0);
        DotsTabView.update(tabs, 5);
    }
}