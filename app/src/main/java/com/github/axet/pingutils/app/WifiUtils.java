package com.github.axet.pingutils.app;

import android.content.Context;
import android.content.pm.PackageManager;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.util.Log;

import com.github.axet.androidlibrary.app.AssetsDexLoader;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;
import java.util.Enumeration;

public class WifiUtils {
    public static String TAG = WifiUtils.class.getSimpleName();

    public static final int WIFI_FREQUENCY_BAND_2GHZ = 2; // WifiManager@hide
    public static final int WIFI_FREQUENCY_BAND_5GHZ = 1; // WifiManager@hide
    public static final int WIFI_FREQUENCY_BAND_AUTO = 0; // WifiManager@hide

    public WifiManager wm;
    PackageManager pm;
    public boolean band24ghz, band5ghz;

    public static int convertFrequencyToChannel(int freq) {
        if (freq >= 2412 && freq <= 2484) {
            return (freq - 2412) / 5 + 1;
        } else if (freq >= 5170 && freq <= 5825) {
            return (freq - 5170) / 5 + 34;
        } else {
            return -1;
        }
    }

    public WifiUtils(Context context) {
        pm = context.getPackageManager();
        wm = (WifiManager) context.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        if (wm != null) {
            try {
                Method m = AssetsDexLoader.getPrivateMethod(wm.getClass(), "getFrequencyBand");
                int band = (int) m.invoke(wm);
                switch (band) {
                    case WIFI_FREQUENCY_BAND_2GHZ:
                        band24ghz = true;
                        break;
                    case WIFI_FREQUENCY_BAND_5GHZ:
                        band5ghz = true;
                        break;
                    case WIFI_FREQUENCY_BAND_AUTO:
                        band24ghz = true;
                        band5ghz = true;
                        break;
                }
            } catch (NoSuchMethodException ignore) {
            } catch (InvocationTargetException ignore) {
            } catch (IllegalAccessException ignore) {
            }
            try {
                Method m = AssetsDexLoader.getPrivateMethod(wm.getClass(), "isDualBandSupported");
                if ((boolean) m.invoke(wm)) {
                    band24ghz = true;
                    band5ghz = true;
                }
            } catch (NoSuchMethodException ignore) {
            } catch (InvocationTargetException ignore) {
            } catch (IllegalAccessException ignore) {
            }
            if (wm.is5GHzBandSupported())
                band5ghz = true;
            else if (isWifiAvalable())
                band24ghz = true;
        }
    }

    public boolean isWifiAvalable() {
        return pm.hasSystemFeature(PackageManager.FEATURE_WIFI);
    }
}
