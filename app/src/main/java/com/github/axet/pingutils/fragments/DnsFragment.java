package com.github.axet.pingutils.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.github.axet.pingutils.R;
import com.github.axet.pingutils.app.DnsServersDetector;

public class DnsFragment extends Fragment {

    private static final String ARG_SECTION_NUMBER = "section_number";
    public static final String NL = "<br/>";

    public static DnsFragment newInstance() {
        DnsFragment fragment = new DnsFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(ARG_SECTION_NUMBER, 0);
        fragment.setArguments(bundle);
        return fragment;
    }

    public static String dns(Context context) {
        DnsServersDetector dns = new DnsServersDetector(context);
        final StringBuilder tt = new StringBuilder();
        DnsServersDetector.Pair[] ss = dns.getServersMethodSystemProperties();
        if (ss != null && ss.length > 0) {
            tt.append("<h2>DNS (System props):</h2>"); // net.dns1, net.dns2, net.dns3, net.dns4
            for (DnsServersDetector.Pair a : ss) {
                tt.append("<b>" + a.name + ":</b> " + a.value);
                tt.append(NL);
            }
        }
        ss = dns.getServersMethodConnectivityManager();
        if (ss != null && ss.length > 0) {
            if (tt.length() > 0) {
                tt.append(NL);
                tt.append(NL);
            }
            tt.append("<h2>DNS:</h2>");
            for (DnsServersDetector.Pair a : ss) {
                tt.append("<b>" + a.name + ":</b> " + a.value);
                tt.append(NL);
            }
        }
        ss = dns.getServersMethodExec();
        if (ss != null && ss.length > 0) {
            if (tt.length() > 0) {
                tt.append(NL);
                tt.append(NL);
            }
            tt.append("<h2>getprop with *.dns1:</h2>");
            for (DnsServersDetector.Pair a : ss) {
                tt.append(a);
                tt.append(NL);
            }
        }
        return tt.toString();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_dns, container, false);

        final TextView textView = (TextView) root.findViewById(R.id.section_label);

        Runnable refresh = new Runnable() {
            @Override
            public void run() {
                textView.setText(Html.fromHtml(dns(getContext())));
            }
        };

        FloatingActionButton fab = (FloatingActionButton) root.findViewById(R.id.fab);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                refresh.run();
                Snackbar.make(view, "Refreshing", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        refresh.run();
        return root;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }
}