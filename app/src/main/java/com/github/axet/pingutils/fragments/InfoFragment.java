package com.github.axet.pingutils.fragments;

import android.content.Context;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.LinkProperties;
import android.net.RouteInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.github.axet.androidlibrary.app.AssetsDexLoader;
import com.github.axet.androidlibrary.app.Storage;
import com.github.axet.pingutils.R;
import com.github.axet.pingutils.app.WifiUtils;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.Inet4Address;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.InterfaceAddress;
import java.net.NetworkInterface;
import java.net.UnknownHostException;
import java.util.Collections;

public class InfoFragment extends Fragment {
    public static String TAG = InfoFragment.class.getSimpleName();

    private static final String ARG_SECTION_NUMBER = "section_number";
    public static final String NL = "<br/>";
    public static final String TAB = "\t";
    public static String[] PERMISSIONS = new String[]{"android.permission.ACCESS_COARSE_LOCATION", "android.permission.ACCESS_FINE_LOCATION"};
    public static int RESULT = 1;

    WifiUtils wifi;

    Runnable refresh;

    public static InfoFragment newInstance() {
        InfoFragment fragment = new InfoFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(ARG_SECTION_NUMBER, 1);
        fragment.setArguments(bundle);
        return fragment;
    }

    public static String formatInterface(InterfaceAddress ia) {
        String str = "";
        InetAddress a = ia.getAddress();
        if (a instanceof Inet4Address)
            str += "inet4 ";
        if (a instanceof Inet6Address)
            str += "inet6 ";
        try {
            str += InetAddress.getByAddress(a.getAddress()).getHostAddress();
        } catch (UnknownHostException e) {
            Log.w(TAG, e);
        }
        str += "/";
        str += ia.getNetworkPrefixLength();
        return str;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        wifi = new WifiUtils(getContext());
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_info, container, false);

        final TextView textView = (TextView) root.findViewById(R.id.section_label);

        refresh = new Runnable() {
            @Override
            public void run() {
                StringBuilder tt = new StringBuilder();

                if (Storage.permitted(InfoFragment.this, PERMISSIONS, RESULT)) {
                    if (wifi.wm != null && wifi.wm.isWifiEnabled()) {
                        WifiInfo wifiInfo = wifi.wm.getConnectionInfo();
                        if (wifiInfo != null) {
                            tt.append("<h2>Wifi");
                            if (wifi.band5ghz || wifi.band24ghz) {
                                tt.append(" (");
                                if (wifi.band24ghz)
                                    tt.append("2.4GHz");
                                if (wifi.band5ghz) {
                                    if (wifi.band24ghz)
                                        tt.append("/");
                                    tt.append("5GHz");
                                }
                                tt.append(")");
                            }
                            tt.append("</h2>");
                            String s = wifiInfo.getSSID();
                            if (s != null && !s.isEmpty())
                                tt.append("<b>").append("SSID: ").append("</b>").append(Html.escapeHtml(s)).append(NL);
                            s = wifiInfo.getBSSID();
                            if (s != null && !s.isEmpty())
                                tt.append("<b>").append("BSSID: ").append("</b>").append(s).append(NL);
                            int f = wifiInfo.getFrequency();
                            if (f > 0) {
                                tt.append("<b>").append("Channel: ").append("</b>").append(WifiUtils.convertFrequencyToChannel(f)).append(NL);
                                tt.append("<b>").append("Freq: ").append("</b>").append(f).append("MHz").append(NL);
                            }
                            tt.append(NL);
                        }
                    }
                }

                String dns = DnsFragment.dns(getContext());
                if (!dns.isEmpty()) {
                    tt.append(dns);
                    tt.append(NL);
                    tt.append(NL);
                }

                tt.append("<h2>Network Interfaces:</h2>");

                try {
                    for (NetworkInterface ni : Collections.list(NetworkInterface.getNetworkInterfaces())) {
                        String str0 = "";
                        String str = "";
                        str0 += "<b>" + ni.getName() + ":" + "</b>";
                        str0 += NL;
                        byte[] bb = ni.getHardwareAddress();
                        if (bb != null) {
                            str += TAB + "mac ";
                            for (byte b : bb)
                                str += String.format("%02X:", b);
                            str = str.substring(0, str.length() - 1);
                            str += NL;
                        }
                        for (InterfaceAddress ia : ni.getInterfaceAddresses()) {
                            str += TAB;
                            str += formatInterface(ia);
                            str += NL;
                        }
                        if (!str.isEmpty()) {
                            tt.append(str0);
                            tt.append(str);
                            tt.append(NL);
                        }
                    }
                } catch (Exception e) {
                    Log.w(TAG, e);
                }

                tt.append("<h2>Routes:</h2>");

                ConnectivityManager cm = (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
                LinkProperties linkProperties = cm.getLinkProperties(cm.getActiveNetwork());
                if (linkProperties != null) {
                    for (RouteInfo routeInfo : linkProperties.getRoutes()) {
                        tt.append(TAB);
                        if (routeInfo.isDefaultRoute())
                            tt.append("<b>");
                        tt.append(routeInfo.getDestination());
                        tt.append(" ");
                        try {
                            InetAddress gw = routeInfo.getGateway();
                            tt.append(" via ");
                            tt.append(InetAddress.getByAddress(gw.getAddress()).getHostAddress());
                            tt.append(" ");
                        } catch (Exception e) {
                            Log.w(TAG, e);
                        }
                        tt.append(" dev ");
                        tt.append(routeInfo.getInterface());
                        if (routeInfo.isDefaultRoute())
                            tt.append("</b>");
                        tt.append(NL);
                    }
                }

                textView.setText(Html.fromHtml(tt.toString()));
            }
        };

        FloatingActionButton fab = (FloatingActionButton) root.findViewById(R.id.fab);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                refresh.run();
            }
        });

        refresh.run();

        return root;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        refresh.run();
    }
}
