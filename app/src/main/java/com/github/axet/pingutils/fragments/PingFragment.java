package com.github.axet.pingutils.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import com.github.axet.androidlibrary.services.WifiKeepService;
import com.github.axet.androidlibrary.widgets.ErrorDialog;
import com.github.axet.androidlibrary.widgets.OpenFileDialog;
import com.github.axet.pingutils.R;
import com.github.axet.pingutils.app.Ping;
import com.github.axet.pingutils.app.PingExt;

public class PingFragment extends Fragment {
    public static String TAG = PingFragment.class.getSimpleName();

    private static final String ARG_SECTION_NUMBER = "section_number";

    Handler handler = new Handler();
    Thread thread;
    Snackbar sb;

    public static PingFragment newInstance() {
        PingFragment fragment = new PingFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(ARG_SECTION_NUMBER, 1);
        fragment.setArguments(bundle);
        return fragment;
    }

    public static Ping newPing(Context context, String host, boolean ipv6) {
        SharedPreferences shared = PreferenceManager.getDefaultSharedPreferences(context);
        if (shared.getBoolean("ping", false))
            return new Ping(host, ipv6);
        else
            return new PingExt(host, ipv6);
    }

    public static void ping(Context context, String host, boolean ipv6, int count, StringBuilder out, Handler handler, Runnable notify) {
        try {
            Ping ping = newPing(context, host, ipv6);
            out.append(String.format("PING %s (%s).", host, ping.ip));
            out.append("\n");
            handler.post(notify);
            int sent = 0;
            int lost = 0;
            double min = Double.MAX_VALUE;
            double max = 0;
            double avg = 0;
            double mdev = 0; // standard deviation
            double tsum = 0;
            double tsum2 = 0;
            long start = System.currentTimeMillis();
            for (int i = 0; i < count; i++) {
                sent++;
                ping.ping();
                double sec = ping.ms / 1000.0;
                if (!ping.fail()) {
                    out.append(String.format("%s (%s): time=%.2f ms", host, ping.ip, sec));
                } else if (ping.unreachable) {
                    lost++;
                    out.append(String.format("Destination unreachable %s (%s): time=%.2f ms", host, ping.hoopIp, sec));
                } else {
                    lost++;
                    out.append(String.format("Timeout %s (%s): time=%.2f ms", host, ping.ip, sec));
                }
                avg += sec;
                tsum += sec;
                tsum2 += sec * sec;
                min = Math.min(min, sec);
                max = Math.max(max, sec);
                out.append("\n");
                handler.post(notify);
                Thread.sleep(1000);
                if (Thread.currentThread().isInterrupted())
                    return;
            }
            avg = avg / sent;
            int recived = sent - lost;
            tsum /= recived + sent;
            tsum2 /= recived + sent;
            mdev = Math.sqrt(tsum2 - tsum * tsum);
            long end = System.currentTimeMillis();
            out.append("\n");
            out.append(String.format("--- %s ping statistics ---", host));
            out.append("\n");
            out.append(String.format("%d packets transmitted, %d received, %d%% packet loss, time %dms", sent, recived, lost / sent, end - start));
            out.append("\n");
            out.append(String.format("rtt min/avg/max/mdev = %.3f/%.3f/%.3f/%.3f ms", min, avg, max, mdev));
            out.append("\n");
            handler.post(notify);
        } catch (Exception e) {
            Log.e(TAG, "Error", e);
            out.setLength(0);
            out.append(ErrorDialog.toMessage(e));
            handler.post(notify);
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_ping, container, false);

        FloatingActionButton fab = (FloatingActionButton) root.findViewById(R.id.fab);

        final EditText edit = (EditText) root.findViewById(R.id.ping_edit);
        final TextView output = (TextView) root.findViewById(R.id.output);
        final CheckBox ipv6 = (CheckBox) root.findViewById(R.id.ipv6);

        if (edit.getText().toString().isEmpty())
            edit.setText(WifiKeepService.getGatewayIP(getContext()));

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                OpenFileDialog.hideKeyboard(getContext(), edit);
                if (thread != null)
                    thread.interrupt();
                thread = new Thread() {
                    @Override
                    public void run() {
                        final StringBuilder out = new StringBuilder();
                        Runnable run = new Runnable() {
                            @Override
                            public void run() {
                                output.setText(out.toString());
                            }
                        };
                        Runnable done = new Runnable() {
                            @Override
                            public void run() {
                                handler.post(run);
                                sb.dismiss();
                            }
                        };
                        String host = edit.getText().toString().trim();
                        ping(getContext(), host, ipv6.isChecked(), 4, out, handler, run);
                        done.run();
                    }
                };
                if (sb != null)
                    sb.dismiss();
                sb = Snackbar.make(view, "Quering", Snackbar.LENGTH_LONG)
                        .setAction("Action", null)
                        .setDuration(Snackbar.LENGTH_INDEFINITE);
                sb.show();
                thread.start();
            }
        });

        return root;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }
}