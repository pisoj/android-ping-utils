# Monitoring

Understanding network issues can be complicated. Internet not working? DNS is down? Wifi dropping packets?

Localication network issues can be done using ping utilities by creating server ping list.

1) First make sure wifi is working, local IP is assigned and DNS assosialocated. All those questions can be answered using Ping Utils INFO tab.

2) Create local servers IP's. For booth protocols IPv4 and IPv5 Those servers has to be responsible for local netowrk issues.

Default gateway local IPv4, IPv6
Default gateway external IPv4, IPv6
Wifi Bridges IP's
Local Printers IP's

3) Create your provider tracerouting tables.

Analyze traceroute output to different internet servers in different regions (Euroe, America, Russia) and see how traffic is managed, record basic routing, record different gateway of your provider. Monitoring network, can also show different provider gateways of different global providers.

4) Add global known IP's from different regions (like global DNS)

google.com IPv4, IPv6
google dns IP's
yandex.ru IPv4, IPv6
yandex dns IP's
